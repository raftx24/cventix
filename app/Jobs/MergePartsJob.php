<?php

namespace App\Jobs;

use App\Models\File;
use App\Service\MergeParts;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MergePartsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private File $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }

    public function handle()
    {
        (new MergeParts($this->file))
            ->handle();
    }
}
