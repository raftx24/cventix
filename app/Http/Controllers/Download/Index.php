<?php

namespace App\Http\Controllers\Download;

use App\Models\File;
use App\Http\Controllers\Controller;
use App\Exceptions\DownloadException;
use Illuminate\Support\Facades\Storage;

class Index extends Controller
{
    public function __invoke(File $file)
    {
        if (! $file->isFinalized()) {
            throw new DownloadException('please first call complete');
        }

        return Storage::disk('uploads')
            ->download($file->saved_name, $file->original_name);
    }
}
