<?php

namespace App\Http\Controllers\Upload;

use App\Models\File;
use App\Http\Requests\CreateRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\File as Resource;

class Create extends Controller
{
    public function __invoke(CreateRequest $request)
    {
        $file = File::create([
            'original_name' => $request->get('name')
        ]);

        return new Resource($file);
    }
}
