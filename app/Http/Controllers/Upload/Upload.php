<?php

namespace App\Http\Controllers\Upload;

use App\Models\File;
use App\Service\Upload as Service;
use App\Http\Controllers\Controller;
use App\Http\Requests\UploadRequest;

class Upload extends Controller
{
    public function __invoke(UploadRequest $request, File $file)
    {
        (new Service($file, $request->file('part'), $request->get('order')))
            ->handle();

        return [
            'status' => 'successfully'
        ];
    }
}
