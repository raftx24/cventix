<?php

namespace App\Http\Controllers\Upload;

use App\Models\File;
use App\Jobs\MergePartsJob;
use App\Http\Controllers\Controller;

class Complete extends Controller
{
    public function __invoke(File $file)
    {
        if ($file->hasParts()) {
           dispatch(new MergePartsJob($file));
        }

        return [
            'status' => 'successfully'
        ];
    }
}
