<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilePart extends Model
{
    protected $fillable = [
        'file_id', 'saved_name', 'order'
    ];
}
