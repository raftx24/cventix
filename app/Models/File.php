<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'original_name', 'saved_name', 'size', 'checksum',
    ];

    public function parts()
    {
        return $this->hasMany(FilePart::class)
            ->orderBy('order');
    }

    public function isFinalized()
    {
        return $this->saved_name !== null;
    }

    public function hasParts()
    {
        return $this->parts->isNotEmpty();
    }
}
