<?php


namespace App\Service;

use App\Models\File;
use App\Models\FilePart;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\UploadException;

class Upload
{
    private File $file;
    private UploadedFile $uploadFile;
    private int $order;

    public function __construct(File $file, UploadedFile $uploadFile, $order)
    {
        $this->file = $file;
        $this->uploadFile = $uploadFile;
        $this->order = $order;
    }

    public function handle()
    {
        if ($this->file->isFinalized()) {
            throw new UploadException('File was finished already, and you cannot upload new parts');
        }

        Storage::disk('uploads')
            ->put('.', $this->uploadFile);

        FilePart::create([
            'order' => $this->order,
            'file_id' => $this->file->id,
            'saved_name' => $this->uploadFile->hashName()
        ]);
    }
}
