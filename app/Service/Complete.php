<?php


namespace App\Service;

use App\Models\File;
use App\Models\FilePart;
use App\Jobs\MergePartsJob;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\UploadException;

class Complete
{
    private File $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }

    public function handle()
    {
        if (! $this->file->hasParts()) {
            throw new UploadException('First you should upload some parts');
        }

        if ($this->file->isFinalized()) {
            throw new UploadException('file already is completed');
        }

        dispatch(new MergePartsJob($this->file));
    }
}
