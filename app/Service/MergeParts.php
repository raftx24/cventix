<?php


namespace App\Service;

use App\Models\File;
use App\Models\FilePart;
use Illuminate\Support\Str;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class MergeParts
{
    private File $file;
    private Filesystem $disk;

    public function __construct(File $file)
    {
        $this->file = $file;
        $this->disk = Storage::disk('uploads');
    }

    public function handle()
    {
        $this->disk->put($mergedFile = Str::random(40), '');

        $this->file->parts->each(function (FilePart $filePart) use ($mergedFile) {
            $this->disk->append(
                $mergedFile,
                $this->disk->get($filePart->saved_name),
                null
            );
        });

        $this->file->update([
           'saved_name' => $mergedFile
        ]);
    }
}
