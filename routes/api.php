<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::name("upload.")->namespace("Upload")->group(function () {

    Route::post('/create', 'Create')
        ->name('create');

    Route::post('/upload/{file}', 'Upload')
        ->name('upload');

    Route::post('/complete/{file}', 'Complete')
        ->name('complete');
});

Route::name("download.")->namespace("Download")->group(function () {
    Route::get('/index/{file}', 'Index')
        ->name('index');
});
