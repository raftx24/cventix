<?php

/** @var Factory $factory */

use App\Models\FilePart;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(FilePart::class, function (Faker $faker) {
    return [
        'saved_name' => $faker->name,
        'order' => $faker->numberBetween(0, 10),
    ];
});
