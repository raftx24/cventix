<?php

namespace Tests\Feature;

use App\Models\File;
use App\Service\Upload;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\UploadException;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UploadTest extends TestCase
{
    use RefreshDatabase;

    private File $file;
    private UploadedFile $uploadFile;
    private Filesystem $fileSystem;

    protected function setUp(): void
    {
        parent::setUp();

        $this->file = factory(File::class)->create();
        $this->uploadFile = UploadedFile::fake()->image('sample');
        $this->fileSystem = Storage::fake('uploads');
    }

    public function testWhenFileUploadedThenShouldFileExistsInTheDisk()
    {
        $this->upload();

        $this->fileSystem
            ->assertExists($this->uploadFile->hashName());
    }

    public function testWhenFileUploadedThenShouldFilePartAddedToDatabase()
    {
        $this->upload();

        $this->assertDatabaseHas('file_parts', [
            'file_id' => $this->file->id,
            'saved_name' => $this->uploadFile->hashName()
        ]);
    }

    public function testWhenFileIsFinalizedThenCannotUploadNewPart()
    {
        $this->expectException(UploadException::class);

        $this->file = factory(File::class)
            ->create(['saved_name' => 'adf']);

        (new Upload($this->file, $this->uploadFile, 0))
            ->handle();
    }

    protected function upload(): void
    {
        $this->json('POST', route('upload.upload', ['file' => $this->file]), [
            'part' => $this->uploadFile,
            'order' => 2
        ]);
    }
}
