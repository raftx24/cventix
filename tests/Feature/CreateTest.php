<?php

namespace Tests\Feature;

use App\Models\File;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateFile()
    {
        $response = $this->post(route('upload.create'), [
            'name' => 'filename'
        ]);

        $response->assertJson([
            'name' => 'filename'
        ]);

        $this->assertDatabaseHas('files', [
            'original_name' => 'filename'
        ]);
    }

    public function testWhenNameIsNotGivenThenShouldNotCreateFile()
    {
        $this->expectException(ValidationException::class);

        $response = $this->post(route('upload.create'), []);

        $response->assertJsonValidationErrors(['name']);
    }

    public function testWhenGivenNameIsDuplicatedThenShouldNotCreateFile()
    {
        $file = factory(File::class)->create();

        $this->expectException(ValidationException::class);

        $response = $this->post(route('upload.create'), [
            'name' => $file->saved_name
        ]);

        $response->assertJsonValidationErrors(['name']);
    }
}
