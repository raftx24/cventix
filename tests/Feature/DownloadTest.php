<?php

namespace Tests\Feature;

use App\Models\File;
use App\Models\FilePart;
use App\Service\Complete;
use App\Jobs\MergePartsJob;
use Illuminate\Support\Facades\Queue;
use App\Exceptions\UploadException;
use App\Exceptions\DownloadException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DownloadTest extends TestCase
{
    use RefreshDatabase;

    private File $file;

    protected function setUp(): void
    {
        parent::setUp();

        $this->file = factory(File::class)->create();
    }

    public function testWhenFileIsNotCompletedThenShouldThrowException()
    {
        $response = $this->get(route('download.index', ['file' => $this->file]));

        $this->assertEquals(DownloadException::class, get_class($response->exception));
    }
}
