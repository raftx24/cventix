<?php

namespace Tests\Feature;

use App\Models\File;
use App\Models\FilePart;
use App\Service\Complete;
use App\Jobs\MergePartsJob;
use Illuminate\Support\Facades\Queue;
use App\Exceptions\UploadException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CompleteTest extends TestCase
{
    use RefreshDatabase;

    private File $file;

    protected function setUp(): void
    {
        parent::setUp();

        $this->file = factory(File::class)->create();
    }

    public function testWhenFileDoesntHavePartsThenShouldThrowException()
    {
        $this->expectException(UploadException::class);

        (new Complete($this->file))
            ->handle();
    }


    public function testWhenFileAlreadyIsCompletedThenShouldThrowException()
    {
        $this->expectException(UploadException::class);

        $this->file->update(['saved_name' => 'abc']);

        factory(FilePart::class)->create([
            'file_id' => $this->file->id
        ]);

        (new Complete($this->file))
            ->handle();
    }

    public function testWhenFileHasPartsThenShouldDispatchMergeFileParts()
    {
        Queue::fake();

        factory(FilePart::class)->create([
            'file_id' => $this->file->id
        ]);

        (new Complete($this->file))
            ->handle();

        Queue::assertPushed(MergePartsJob::class);
    }
}
