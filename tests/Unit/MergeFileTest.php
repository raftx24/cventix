<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\File;
use App\Models\FilePart;
use App\Service\MergeParts;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MergeFileTest extends TestCase
{
    use RefreshDatabase;

    private File $file;
    private UploadedFile $uploadFile;
    private Filesystem $disk;

    protected function setUp(): void
    {
        parent::setUp();

        $this->file = factory(File::class)->create();
        $this->uploadFile = UploadedFile::fake()->image('sample');
        $this->disk = Storage::fake('uploads');
    }

    public function testMergeTextFiles()
    {
        $this->createPart(0, UploadedFile::fake()->createWithContent('part1', 'ABC'));
        $this->createPart(1, UploadedFile::fake()->createWithContent('part2', 'EFG'));

        (new MergeParts($this->file))
            ->handle();

        $this->assertEquals('ABCEFG', $this->disk->get($this->file->saved_name));
    }

    public function testMergeBinaryFiles()
    {
        $result = $this->createPart(0, UploadedFile::fake()->image('img1.jpeg'));
        $result .= $this->createPart(1, UploadedFile::fake()->image('img2.jpeg'));

        (new MergeParts($this->file))
            ->handle();

        $this->assertEquals(
            base64_encode($result),
            base64_encode($this->disk->get($this->file->saved_name))
        );
    }

    protected function createPart($order, UploadedFile $file)
    {
        factory(FilePart::class)->create([
            'file_id' => $this->file->id,
            'saved_name' => $file->hashName(),
            'order' => $order,
        ]);

        $this->disk->put('.', $file);

        return $file->get();
    }
}
